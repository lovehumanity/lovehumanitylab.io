---
title: "Black and White Thinking"
date: 2021-02-20
classes: wide
categories:
  - Lessons
header:
  teaser: "/assets/blogs/2021-02-20-Black/black.jpg"
---

Black and white thinking is essentially seeing things or people as good or bad and not realising that there are so many blends of grey that you can't just put things or people into boxes. I should also say that its only rarely things are black and white.

To get past black and white thinking you should just accept that there are many areas of grey and people/things are not good or bad, they are more than that. It also leads to a wrong perception of the world when you think in terms of black and white.

Its a very unhealthy habbit of seeing people as good or bad which I'm somewhat getting better at. It also plays into the idea that 'people are against you', which is an unhealthy attitude to have. No doubt in life you will/do have people against you, but to generalise is not good. 

Finally, black and white thinking is something which leads people down the wrong road at times, because you push people you see as bad and try to only get to the 'good' people and so live a lonely life. It also has the impact of pushing away people who are just trying to help you, which isn't good. Don't allow this to happen! Know that there are many shades of grey with people and things and so you can't just label them good or bad. Peace and God bless
