---
title: "Showing Respect and Love to God and his Kingdom"
date: 2021-08-14
classes: wide
categories:
  - Important
---

![Kingdom](/assets/blogs/2021/kingdom/kingdom.jpeg)

Quick note 15/05/24

Spent a long time playing around with different images for this article because this article is one of the BEST on this blog. I used a yellow gateway image online that I didn't like for some time, then I used an AI generated image, because I believed it to be very good and now I am using one of my abstract pieces that doesn't have a name because I believed it to be one of my best and this art I have made has no name because it is one of my best pieces as an artist. 

Enjoy reading!

The first thing that I think is quite important in showing respect and love to God and his kingdom is to show kindness, understanding, forgiveness, love, compassion, and respect to all of Gods creation regardless of who or what they are. This could be paying respect to a flower by providing it water, this could be forgiving someone that did wrong by you. It could also be paying respect to the planet by recycling your bottles and tins etc. This also applies to yourself because whether you like it or not you are apart of Gods kingdom, therefore treat yourself with the same amount of love and respect as you would God. Everything and everyone have a place in Gods eyes and everything and everyone is equal according to God.

Another thing that is quite important in showing love and respect to God and his kingdom is to not give way to dark, evil, bad thoughts because you may hide such thoughts from your colleagues and family, but you will not be able to hide such thoughts from God. Also related to this is to not give way to unhappiness and misery because you are blessed with the life that God has given you and so should strive to be happy and cheerful as a show of thanks towards God for the life that he has given you, because he could have chosen to not create you but he did so out of pure selflessness.

Prayer and meditation is another thing which is important for paying respect to God and his kingdom because through meditation and prayer you can begin to build a relationship with God and also lessen the effects of ego which is dispositions and thoughts related to yourself, in other words thinking for yourself and not for others. Its important to realize that it’s not all about you – there are other people in your life that are affected by your actions. Related to this is providing some kind of service to humanity, this could be through your job, through writing blog posts to spread your knowledge to the world, this could be helping your parents with household work etc. 

Lastly another means of showing love and respect towards God and his kingdom is by having faith that God will regardless of the circumstance provide for you, and this is something that I have personally struggled with because its difficult having faith that God will provide when things really are bad, in that it requires a lot of faith in God to go through hardship and difficult situations. Which is why its important in my opinion to when things don’t go your way to not see it as a bad thing because ultimately you don’t know if its really a bad thing as you can’t see into the future. Point being is that you shouldn’t get let down when things don’t go according to the way you would like them to, just see them as blessings in disguise. 

I also recommend that you follow the 10 commandments as these are principals that have been understood by the wisest amongst us and should be followed. With “thou shall not kill” also seek to understand the true meaning behind it, when you put someone’s idea down are you not killing their ambitions? Are you not killing their confidence to build things from their ideas? I leave you to ponder the full meaning behind “thou shall not kill”. 

