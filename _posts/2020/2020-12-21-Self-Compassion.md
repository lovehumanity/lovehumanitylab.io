---
title: "Self-Compassion"
date: 2020-12-21
classes: wide
categories:
  - Christ
header:
  teaser: "/assets/blogs/2020-12-21-Self-Compassion/compassion.jpg"
---

![Compassion](/assets/blogs/Christ/Compassion.jpeg)

I have experienced a lot of hardship and difficulty during my life. One thing I learned is that self-compassion is the needed antidote during times of hardship and difficulty. Life has ups and downs. For the ups to be experienced you need to go through the downs. During such times its important to learn and practice self-compassion. 

I define self-compassion as the practice of seeing the person who is going through the hardship and difficulty and having a strong level of empathy and appreciation for that suffering. Its being able to see that it is tough. But its also removing any self-blame you have that you are the reason for your hardship and difficulties. Its being able to see yourself remote from the 'I' and seeing yourself just as another person of the world who is going through tough times in life. Its also seperating the suffering from the person. 

The greater the hardship and suffering, the greater the level of compassion needed. The Buddha also recognised the importance of compassion; "If your compassion does not include yourself, it is incomplete".

It goes without saying that everyone has their own level of hardship they have endured, some people have gone through some serious heartbreak and trauma, while others have had an easier ride through life. But for there to be compassion, there needs to have been difficulty and hardship along the way. Most people have gone through hard times, self-compassion is vital to ensure that you can get through such times.  

In conclusion, the only value that you can get out of reading this is by you taking action yourself and actually putting to practice what I preach here. Only you can give yourself self-compassion, nobody can do it for you. Not your sister, not your parents, not your friends. You have to be the one to do it.  


