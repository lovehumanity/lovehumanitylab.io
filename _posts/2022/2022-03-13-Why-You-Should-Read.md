---
title: "Why You Should Read"
date: 2022-03-13
classes: wide
categories:
  - Writing
header: 
  teaser: /assets/blogs/reading/reading.jpg 
---

![Reading]({{ site.url }}{{ site.baseurl }}/assets/blogs/reading/reading.jpg) 

In this article I will be talking about reading and why its so important to take up reading and have it be a part of your daily routine. 

Now reading doesn’t necessarily need to just be books that your read – it can also be reading about research papers in your relevant field, it can be reading articles, it can be reading blogs as you are doing right now – it isn’t only books that I encourage you to read, its any content that will bring value to you in your life.

I was so inspired by the film Good Will Hunting and in the film the main character Will who was played by Matt Damon reads a considerable amount of books in his free time and there was one scene in particular which really did inspire me and that was when he was in the therapists room and he saw all the books on the shelves and he said that he read all the books on the shelves as if it was nothing to him. 

This amazed me and really encouraged me and inspired me to take up reading and have that be apart of my life.

One of the reasons why you should take up reading is because it allows you to become a more knowledgeable and sophisticated person and allows you to develop your vocabulary and allows you to know more about the world (especially if you are reading good books). 

But you should only read because you want to read – don’t read just because I say it here in this article, read because you want to read and only read the content that you feel drawn to and content that you are interested in.

Reading also in my view allows you to become a lot better and a lot more developed person because it allows you to have a much clearer picture on the world, especially if you are reading the correct content – I can not emphasis this enough, because why should you read a book from someone who has 1 year experience in x subject when you can read a book written by someone who has 30 years of experience and has worked at multiple different companies? 

Point being is you should be selective of the content that you consume and only consume content which is of high value from people who know what they are talking about.

Another short point I just wanted to add here is that I believe that you shouldn’t read the news too often because the news has just become so heavily politicized especially with the virus always on the news and being spoken about. It just isn’t healthy for your mental health to constantly be watching the news every single day because bad news sells a lot more than good news and also how much value does the news really bring you in your life? 

Can you recall 5 articles that you read over the past month? 

I wager that most will say that they are not able to. 

Only take up reading because you want to take up reading and become the best version of yourself and only consume content which you feel drawn to and content which is of high value objectively speaking from people who are good writers/experts in their fields etc, because it will allow you to in one book read from an expert and this is of more value in my eyes than reading 100 books from people with very little experience in a subject. 

You also shouldn’t read content just because they are the most popular – granted most popular does mean something, but you should read content which you believe will add value to you in your life and content which is of high quality. 

But its also about the fact that you shouldn’t just read what everyone else is reading because how unique are you really if you just read everyone else’s thing which they are reading?
