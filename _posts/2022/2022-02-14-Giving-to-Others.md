---
title: "Giving to Others"
date: 2022-02-14
classes: wide
categories:
  - Practice
--- 

![Giving](/assets/blogs/2024/Giving To Others/Giving to Others.jpeg)

Everyone has the ability to give and it’s important that others give because as the Master Jesus once said you ‘reap what you sow’, therefore what you put out to the world you will also get in return, but only if you believe that God will return what you give and you shouldn’t worry too much about when God will return what you give because so long as you have enough faith that God will give what you put out – he will.

Its important also to not have ill will when giving to others but have the real intention of helping them and doing it for them and not for yourself, and having trust that God will return to you in his own time whatever it is you give out to the world.

A basic example is when you pray for others with the intent of helping them, then God will see that and will return that and help you in some way. 

Its about the value that the giving brings to the other person which is of importance and the benefit it brings to that person. 

Its also about the level of sacrafice that you give which is of importance. A basic example of this is you really value a possession of some kind and give that away then that confers greater blessings than giving away items that have no value to you. 

But you should also appreciate the value that items bring to others and appreciate the value that the items bring to yourself, a basic example for me is I use my PC for work and for me to give that away to a child who will never use it will be detrimental to both parties in the long run.

Giving is also about not just giving away money because there are so many other ways that you can give to others in a way which is beneificial to them. Giving can be giving away your knowledge to the world by teaching it to others, starting a blog like I have done and sharing it online to others for their benefit and for yours. 

Giving can also be giving away material possessions to others in a way which brings them benefit, espcially if the item isn't being used. But you can also lend your items out which are not being used which is a form of giving. 

But only give if you believe that you would benefit and the other party whom you are sharing with will benefit.

Lastly, it may take some time for whatever you gave to return to you, but just have faifth that God will return to you whatever it is that you gave and go about your day in the knowledge that you have helped someone in a small way and enjoy that feeling. 

Of course this article will not be worth anything unless it is applied to you and your own life as with every single one of my articles, so please do take into account this valuable teaching I have given you here and try it for yourself and see the truth in these words for yourself.

From my own experience, you receive blessings to the level of sacrafice that you have taken in terms of what you gave. I had only £10 in my wallet and I gave it to my sister, this was quite a big sacrafice because that was the only note that I had.

It also makes you a lot more cheerful when you give to others, so seek to make giving a part of your life.

There was also a really good video on YouTube that said that giving to others will make you a lot happier and a lot more cheerful [Ted Talk](https://www.youtube.com/watch?v=78nsxRxbf4w)
