---
title: "Meditation"
date: 2022-02-19
classes: wide
categories:
  - Practice
header:
  teaser: /assets/images/monk1.jpg
---

![monk](/assets/images/monk.jpg)

Meditation, prayer and service to man is of vital importance in making real progress towards God and progress in spirituality.

The benefits of meditation are you become a more developed human being - you develop your auras and allow your chakras to become balanced. You become more peaceful and you gain more insight into yourself.

You should also seek to meditate in secret.

Now for the technique on how to meditate correctly.

Firstly when you start meditating its best to choose a quiet place where you can not be disturbed by others and where there is little outside noise and distractions.

Secondly you should say a short prayer thanking God for his protection and thanking Angels and your guides for watching over you as you meditate.

Thirdly you should visualise yourself using your hands placing a white cocoon over yourself. 

The actual meditation is simply trying to clearing your mind.

I can't emphasise how difficult this is to do.

Buddhists call it the "monkey mind".

It takes a lot of practice to be able to clear your mind.

Don't be discouraged, keep at it and you will get better.

You can meditate for as long or as little as possible.

However, for those starting, I recommend 15 minutes daily, perhpas evening or morning times and this is the length of meditation I have done for years.

I will also add that you should seek to go deeper within while you clear your mind.

After you have finished meditation you should send this energy which you have received from God out to the world - just imagine yourself sending out this energy is all you need do. 

Then thank Gods angels and your guides for watching over you and then imagine yourself lowering this cocoon that you had placed over you.

Aim to be consistent. 

You can also meditate on anything you choose to.

In the past I have meditated on things such as love, peace, mountains, happiness.

Equally you don't need to meditate on anything.

In fact some people have learned to be in a constant state of meditation and so are always connected to peace while they do their daily things such as work etc.

This is difficult to do, so generally I advice beginners to do a meditation in a quiet place without distractions.

I will say that I have been meditating fairly consistently for close to 6 years now.

Its made be more peaceful and I've gained more insight into myself.

I also wish to mention another technique for mediation and this is a lot more advanced that blanking the mind, so my advice is to continue that practice until you've mastered it and it takes years to master than one simple skill of being able to blank your mind.

The technique for more advanced people is visualisation.

This is quite simply being able to clearly picture an image or an object in your minds eye and this is a very difficult skill to learn.

Our imagination is a great tool, but hard to control as such it is difficult to be able to master our imagination.

