---
title: "The Ten Commandments"
date: 2022-04-15
classes: wide
categories:
  - Important
header: 
  teaser: /assets/blogs/ten/ten.jpg 
---

1. Thou Shall Have No Other Gods Before Me
2. Thou Shall Not Make for Yourself an Idol
3. Thou Shall Not Take the Name Of God In Vein
4. Thou Shall Remeber the Sabbath Day To Keep it Holy
5. Thou Shall Honour your Father and Mother
6. Thou Shall Not Kill
7. Thou Shall Not Commit Adultery
8. Thou Shall Not Steal
9. Thou Shall Not Bear False Witness
10. Thou Shall Not Covet

I wanted to write this short piece on the ten commandments.

The ten commandments were given to us through the prophet moses and have been understood by the wisest of the wise and if adhered to and really understood are enough to carry you into the arms of God. 

The ten commandments paint a picture of one taking accountability for one's own life and the student should seek to live by these commandments. 

When adhering to the ten commandments, seek to really meditate and spend some time understanding each of them and their implications and how to apply them into your life. 

I also recommend you recite the 10 commandments by hand and do lines with them and put efforts to contemplate in which ways you break the 10 commandments.

If you really follow the ten commandments you would be on par with the master.

There are many ways that you can interpret the 10 commandments, but suffice is to say that you should make up your own interpretations of the ten commandments and then seek to live by them.