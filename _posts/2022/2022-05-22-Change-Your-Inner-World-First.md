---
title: "Change Your Inner World First"
date: 2022-05-22
classes: wide
categories:
  - Important
--- 

In this short article I will talk about how you should seek to pray for your inner world and seek to change your inner world before you attempt to pray for others outside your inner world.

We all live in our own worlds. No person’s world is the same as another person’s, but I believe that its very important for those on the spiritual path towards God to seek to change the things in their inner world which is causing them harm before attempting to change other people’s worlds who are not in your world.

A basic example of this is that you should seek to pray for those close to you who are causing you harm before you seek to pray for those outside your inner world who are causing others harm. In a sense this is very much the analogy of when you are on a plane and before takeoff the flight attendant says in an event of an emergency for you to put your gasmask on before you put on the gasmask of those near you. The reason for this is that you should ensure that you are in a good place and are actually fit enough to help others, before you seek to help others.

This is very much why its important to pray for your inner world who are causing you harm in your inner world, before seeking to help those who are outside your inner world. 

However, its also important to pray for yourself for your own benefit before seeking to pray for those in your inner world causing you harm, because again if you have a lot of issues and wounds from the past which are weighing you down mentally, emotionally, and spiritually, then you are not able to pray for others with the intention of helping them. 
