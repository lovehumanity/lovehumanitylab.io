---
title: "The Life Plan"
date: 2024-02-16
classes: wide
categories:
  - Lessons
---

What many on earth do not know is that before we incarnate here we choose what is referred to as a life plan and there are a range of different life plans and while on earth we all have a life plan, not just us, but all the animals, plants, rocks, even planet earth itself has what is referred to as a life plan.

A life plan is quite simply a roadmap of certain sets of experiences that have been mapped for you in your life, that is quite simply all they are.

We also have at least one guide guiding us throughout our entire lives, who watches over us in the background so to speak to ensure that we stick to our life plan, some people can have more, but generally speaking most people have just one.

However it is rare, but from time to time, some people can decide to change their life plan for various reasons, in my case I changed my life plan due to becoming  a little bit more advanced. 

In which case I received a new life plan and a guide is guiding me to ensure I stick to it.

I also want to briefly mention here, that it can happen that you advance higher than your own guide and so then a different more advanced guide would then guide you in your life, but generally speaking most people stick with their one guide their entire lives.
