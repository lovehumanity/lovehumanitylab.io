---
title: "On God"
date: 2024-03-01
classes: wide
categories:
  - Lessons
---

On God.

God wants good for all.

No exception.

Even the worst of sinners.

He wants good for all.

We are all apart of God's Kingdom.

We are all in this together.

Consider it from God's position.

He has to put up with the most atrocious of sinners.

The worst kinds of people beyond imagination.

How do you think that makes God feel?

Seeing the most atrocious kinds of sinners in his kingdom?

People that literally don't even value their own existence.

An important thing to realize is this.

We've been given life.

God gave us life.

The least we can do is pay respect to the very life that we've been given.

God wants good for us all.

No exception.

Seek to do good, if not for God, then do it for yourself.

If not for yourself, then do it for others.

What would you do if you were in God's shoes and you had to put up with sinners beyond imagination?

