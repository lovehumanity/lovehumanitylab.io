---
title: "Social Media"
date: 2024-02-27
classes: wide
categories:
  - Lessons
---

Social Media.

People spend many hours each day on social media.

Mindlessly scrolling on their feed...

Hoping to find that treasure...

That...

Thing, that they are looking for.

Seek to use social media in a better way.

Don't spend 6 hours of your day on social media.

Be careful of the content you consume each day.

Seek to get use out of it.

But don't let it control you in your life.

Seek to follow people who are wise.

Seek to increase the "Spiritual Bank Account".

But do it for you.

Nobody else.
