---
title: "The Gifts of the Spirit"
date: 2024-02-16
classes: wide
categories:
  - Lessons
---

I had a similar article on this blog previously back in 2022, but I've remade it to make this subject more clear.

What many people don't know in the world is that we humans are a lot more than just the five senses or our bodies, we are in fact a spiritual force.

Therefore, we have a number of gifts available to us that come from God, however for the majority of people they lay dorment.

Through meditation, prayer and service we can develop the gifts of the spirit.

Usually it takes years of these practices to receive the gifts from God because God grants the gifts freely and any misuse, he can just as easily take them away 

What are the gifts of the spirit?

In short:

- Telepathy, the ability to communicate by just our thoughts to others 
- Telekinesis, the ability to move objects with our minds
- Clairsentience, the ability to feel with our spiritual senses 
- Clairvoyance, the ability to see into the spiritual realm
- Teleportation, the ability to move from one location to another through visualisation

But I must emphasis, that the only way to obtain the gifts is through meditation, prayer and service.

I will also say that to develop them it takes skilled teachers and guides to help you learn them, don't try to develop them yourself as it is dangerous, normally they come in time through consistent meditation, prayer and service.

Also, don't meditate, pray and give service just to receive them, it doesn't work like that.

They are suppose to be used to help people, however there are black magicians, as they are so known that use the gifts for the detriment of others, however they will pay a price in remorse for the harm they cause, therefore it's best to use them to help others.

However, I will just add that if you feel drawn to black magic or evil, don't deny yourself the experience just because of what I say here, there is still much to be learned from such practices. 

But just be aware that you will one day need to pay back in remorse the harm you've caused.

I will also mention that this information comes from The Great White Brotherhood.

They are a reliable source for spiritual teachings and I have benefitted greatly from their teachings.

They also have a lot of really good books on spirituality that I have grown a lot from. I recommend starting with their first book "The Stairway to Freedom" for those starting their journey.

<https://thegreatwhitebrotherhood.org/books/>
