---
title: "Warnings for the Spiritual Student"
date: 2024-02-18
classes: wide
categories:
  - Lessons
---

There are some things to be aware of for the spiritual student to know of that may be useful.

Generally those not spiritually awake will sense an enlightened one and will use whatever means they have at their disposition to discredit the student, this may come in the form of verbal abuse, dishonouring the student, the student may even suffer physical abuse in some countries.

It's important generally to either "turn the other cheek", or "turn to them the right cheek also" it's up to the student when they should do either.

It's important also to know that you should practice spirituality in secret, I have an [article]({% post_url 2021/2021-09-25-Practice-Spirtuality-in-secret %}) about this on my blog.

Furthermore, you should know that the dark forces or evil forces in the world act automatically and they will do whatever they can to ensure that things like meditation groups, prayer groups or anything of that nature is put to a quick stop, so the general advice is to play the game by their rules, ensure that what tou are doing is legal, that you are on good terms with whatever society you live in.

It's important to know also that once you awaken spiritually, there will be a period of when depression sets in as you come to realise that there is a lot more to life. 

There may also be decline of mental health and you may even experience a mental breakdown in some instances.

The spiritual path should also be mapped out and it's a linear approach.

What's more, when you start to meditate and pray, your ego will be reduced but you will begin receiving spiritual power from God, and there will be a period whereby the spiritual power is being used to replace the lesser power of the ego as such its normal for the body to experience a weakened immune system and so you may experience more viruses, more colds etc, for example I got Covid while doing a lot more prayer and meditations and recently I got a flu while starting to meditate and pray again.

The general advice is to take your time and don't worry too much about the effect it has on the body, but just make sure you do it linearly and if you notice decline of physical health then perhaps meditate and pray less.
