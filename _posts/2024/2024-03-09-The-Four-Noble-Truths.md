---
title: "The Four Noble Truths"
date: 2024-03-07
classes: wide
categories:
  - Buddhism
---

Meditate on these truths.

Pray for all to understand these truths so that all suffer less and it therefore helps you to suffer less.

It takes years for those on the spiritual path to reach enlightnement and enlightnement is a gradual process and you receive tibets of enlightenment into yourself after each meditation session.

Please do read my article on this blog on the correct way to meditate and pray.

## The First Noble Truth

### The Three Marks of Existence

- There is dukka, suffering, unsatisfactoryness, stress
- There is existence and non-exitence, put another way, there is something and there is nothing
- There is the Self, made up of five things; Form, Perceptions, Feelings, Sensations, Conciousness

### Types of suffering

- Dukkha-dukkha, Suffering of suffering, You suffer because you want satisfactory or permanent physical and emotional comfort 
- Viparinama-dukkha, The Suffering of change, You suffer because you are unable to accept change as a natural part of existence
- Sankhara-dukkha, The Suffering of existence, You suffer simply because of existence 

### Karma

The good you do in the world, you get in return.

Karma is made up of 3 aspects

- Body, made up of killing, stealing, sexual misconduct
- Speech, made up of lying, slander, harsh speech, idle gossip
- Mind, made up of greed, anger and delusion

To have good karma its important to avoid doing anything related to these things I mention here.

Reincarnation exists.

The good we do in this life, affects the life we receive in the next life

Desire exists.

## The Second Noble Truth

The cause of Dukka/Stress/Suffering is arising

Arising can take the form of:

- Craving/Wanting physical and mental sensations 
- Craving/Wanting existence, for there to be something.
- Craving/Wanting non-existence, for there to be nothing.

## The Third Noble Truth

What is subject to arising, is subject to ceasing.

Put simply, if it has a beggining, it has an end.

Craving/Wanting has a beginning, therefore it must have an end.

## The Fourth Noble Truth

Firstly it need be understood that there is an end to suffering

The Eightfold Path

The path leading to the end of suffering

One can also use the word "perfect" or "correct" when describing the eightfold path and they can be understood in any order at all.

The Eightfold Path

Right View - Seeing things as they truly are, having a perfect understanding of the four noble truths

Right Intention - Doing what is right for yourself and others

Right Speech - Knowing what to say and when to say it

Right Action - Doing things in the correct manner

Right Livelihood - Doing the right work

Right Mindfulness - Having the correct mindfulness

Right Concentration - Having the correct concentration

Right Effort - Having the correct Effort