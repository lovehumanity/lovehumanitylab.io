---
title: "Mindfulness"
date: 2024-05-14
classes: wide
categories:
  - Practice
---

![Mindfulness](/assets/blogs/2024/mindfulness/mindfulness.jpeg)

Ah mindfulness.

What can I tell you about being mindful.

Mindful of what exactly?

Minfull of your mind?

Mindful of your thoughts?

Mindful of your breathing?

What exactly is being mindful?

Mindfulness involves noticing.

Seeing and oberserving.

Being aware of things in a different manner.

Set aside a daily mindfulness practice of an hour a day and set an alarm each day to practice your mindfulness practice for an hour or less if you prefer.

This way you will never forget to practice mindfulness.

It has been said my a renowned Buddhist teacher that its easy to be mindful, but difficult to remember to be.

Doing what I suggest, you will never forget to be.