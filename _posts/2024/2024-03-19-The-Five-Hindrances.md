---
title: "The Five Hindrances"
date: 2024-03-18
classes: wide
categories:
  - Buddhism
---

The Five Hindrances are natural states of the mind.

In order to reach nirvana it is necessary to understand these five hindrances.

## The Five Hindrances

These are tendencies of the mind

Overcoming them requires that the person practice mindfulness and recognize them when they appear in daily life.

Sensory desire - Wanting input from the five senses.

Ill will - Wanting to always harm or be averse

Sloth and Torpor - Wanting to always slump into the abyss 

Restlessness and Worry - Wanting to always move or have reason to worry

Doubt - Wanting to always have some reason to believe otherwise