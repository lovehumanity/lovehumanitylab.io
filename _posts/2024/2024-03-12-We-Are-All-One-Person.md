---
title: "We Are All One Person"
date: 2024-03-12
classes: wide
categories:
  - Important
---

We are all one person.

This is spiritual truth.

This one person is divided into all we know today.

All the people you know, all the animals you know, all the plants you know, all the minerals you know.

Is in fact **versions** of this one person or aspects of this one person.

The God spirit that resides in I is the same God spirit that resides in you.

We are all the same person.

We are work for the same person.

That person being I as much as it you.