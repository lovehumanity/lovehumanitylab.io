---
title: "Happiness"
date: 2024-03-02
classes: wide
categories:
  - Lessons
---

![Happiness](/assets/blogs/2024/Happiness/Happiness.jpg)

Happiness.

Something some have more than others.

Happiness is an emotion.

As such it can be incorporated within us.

Just like any other emotion.

How do we become happy in our lives?

The first most important thing to do.

Is refrain from negativity and unhappiness.

The next thing is to seek to be as postive as you can be.

Whatever the problem, no matter how great.

Always seek to be as postive as you can be.

A suggestion is to also when you wake up in the morning.

Say a short prayer to God.

Thanking him for placing you in his hands and having his angels watching over you throughout the day.

Seek to do this daily in your life.

Never give way to unhappiness or negativity in your life.

If you do this each day you will become very happy in your life.

Update 1: 25/03/2024

Some things that have helped me be positive is

- Focussing more on the positive
- Imagining being positive 

Its also important to put efforts to reject unhappiness and always aim to be as positive as you can be.

Meditation also helps in becoming happy generally.