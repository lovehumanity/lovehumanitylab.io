---
title: "Setting up Meditation and Prayer Groups"
date: 2024-02-18
classes: wide
categories:
  - Practice
---

I wanted to write here that it's generally advisable for those seeking value in the spiritual bank account that you should perhaps think about setting up meditation/prayer/spiritual groups of some sort.

Keep it simple, a small gathering place in a local community center with cheap advertising online and posters perhaps will be very good.

However be warned, that you need play the game by the society's you live in laws/regulations/rules and be a council upon yourself when deciding to do so.

Meditate on if you feel ready to do such things.