---
title:  "About"
layout: archive
permalink: /About/
classes: wide
---

Link to the gitlab history of his blog <https://gitlab.com/lovehumanity/lovehumanity.gitlab.io> for all to read its own history since the blog's inception.

Please take some time on this blog and read the articles.

This Blog started in early 2020s with the main aim of documenting and sharing with the world my spiritual journey and progress.  

I encourage all who see this blog to share it with anyone that would benefit from it. 

[RSS Feed](https://lovehumanity.gitlab.io/feed.xml)

I hope to have all articles be timeless, so that regardless of the time period you find yourself in, the articles in this blog hold value. 

All the content on this blog was made by myself.

Please do read my article linked [Aims For This Blog and Removal of Various Articles](/aims-for-this-blog-and-removal-of-various-articles/)

I am currently reviewing each article on this blog for quality and timelessness so will be amending each article one at a time.

I am also seeking to get this blog indexed on search engines as it currently isn't indexed very well on things like Google and am seeking outside support for this.

Stay tuned for that!

Last Updated 01/09/24